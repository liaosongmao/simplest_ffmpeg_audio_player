﻿## 最简单的基于FFmpeg的音频播放器 2 (SDL 2.0)

#### 作者介绍
+ 雷霄骅 Lei Xiaohua leixiaohua1020@126.com
+ 中国传媒大学/数字电视技术
+ Communication University of China / Digital TV Technology
+ http://blog.csdn.net/leixiaohua1020



#### 环境介绍
+ Windows 10
+ Visual Studio Community 2015 

#### 项目介绍
本程序实现了音频的解码和播放。是最简单的FFmpeg音频解码方面的教程。通过学习本例子可以了解FFmpeg的解码流程。
项目包含3个工程：
+ simplest_ffmpeg_audio_player：基于FFmpeg+SDL的音频解码器
+ simplest_ffmpeg_audio_decoder：音频解码器。使用了libavcodec和libavformat。
+ simplest_audio_play_sdl2：使用SDL2播放PCM采样数据的例子。
    + 该版本使用SDL 2.0替换了第一个版本中的SDL 1.0。
    + 注意：SDL 2.0中音频解码的API并无变化。唯一变化的地方在于其回调函数的中的Audio Buffer并没有完全初始化，需要手动初始化。本例子中即SDL_memset(stream, 0, len);

